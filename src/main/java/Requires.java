import java.time.LocalDate;

public class Requires {
    public static class Str{
        public static void NotNullOrEmpty(String fieldValue, String field) {
            if (fieldValue == null || fieldValue.isEmpty()) {
                throw new IllegalArgumentException(field + " should not be null or Empty");
            }
        }
    }
    public static class DateTime{
        public static void NotNull(LocalDate localDate, String fieldName) {
            if (localDate == null) {
                throw new IllegalArgumentException(fieldName+ " should not be null");
            }
        }
        public static void NotFuture(LocalDate localDate, String fieldName) {
            boolean isFutureDate = localDate.isAfter(LocalDate.now());
            if(isFutureDate){
                throw new FutureBirthdayException(localDate, fieldName);
            }
        }
    }
}
